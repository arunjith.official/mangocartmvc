class CartModel {
    constructor() {
        this.noOfMangos=0
    }
    addMangos () {
        this.noOfMangos++
    }
    removeMangos () {
        this.noOfMangos--
    }
    getnoOfMangos  () {
        return this.noOfMangos
    }
}

export default CartModel