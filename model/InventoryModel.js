class InventoryModel {
    constructor() {
        this.noOfMangosInStock=10;
    }
    getStock () {
        return this.noOfMangosInStock;
    }
    removeFromStock () {
        this.noOfMangosInStock--
    }
    addToStock () {
        this.noOfMangosInStock++
    }
}

export default InventoryModel