import CartModel from "../model/CartModel.js";
import InventoryModel from "../model/InventoryModel.js";
const cartModel= new CartModel();
const inventory= new InventoryModel ();
class CartController {
    constructor() {
    }
    addOneMango () {
        if (inventory.getStock () !==0) {
            cartModel.addMangos()
            inventory.removeFromStock()
            console.log("your cart has "+cartModel.getnoOfMangos()+" mangos");
            console.log(inventory.getStock () +" available")
        }
        else{
            console.log("stock out")
        }
    }

    removeOneMango () {
        if (cartModel.getnoOfMangos() !==0 ) {
            cartModel.removeMangos();
            inventory.addToStock();
            console.log("your cart has "+cartModel.getnoOfMangos() + " mangos")
            console.log(inventory.getStock() +" available")
        }
        else {
            console.log("your cart is empty")
        }
    }
}
export default CartController